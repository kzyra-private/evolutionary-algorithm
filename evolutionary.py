"""
========================================================================================
    TRAVELLING SALESMAN PROBLEM

    Proper way to execute a script: python3 ./evolutionary.py map size mut_chance mut_range

map - choosen map (city distribution) <int>
    0 - regular
    1 - random
    2 - groups of cities
size - size of population <int>
mut_chance - probability of being mutated <float>
mut_range - range of mutation/number of changed cities <float>

========================================================================================
"""

import sys
import time
import random
import matplotlib.pyplot as plt
from scipy.spatial import distance

"""
========================================================================================
CHECKING CORRECTNESS OF PASSED ARGUMENTS
========================================================================================
"""


def check_args():
    if len(sys.argv) != 5:
        print('Error: Invalid number of arguments. Try: python3 ./evolutionary.py map size mutation_chance')
        print('Note that all passed arguments should be numbers.')
        sys.exit()
    elif int(sys.argv[1]) > 3 or int(sys.argv[2]) < 30 or float(sys.argv[3]) > 1:
        print('Error: Argument out of range.')
        sys.exit()


"""
========================================================================================
GENERATION -> MAKING STARTING POPULATION
========================================================================================
"""


def generate_population(size):
    population = []
    genes = 'xYyZzAaBbCcDdEeFfGgHhIiJjKkLl'

    for j in range(0, size):
        track = "".join(random.sample(genes, len(genes)))
        population.append([track, 0])

    return population


"""
========================================================================================
EVALUATION - > calculate length of a track for each element
========================================================================================
"""


def count_length(cities, specimen):
    track_len = 0

    for i in range(0, len(specimen[0]) - 1):
        track_len = track_len + distance.euclidean(cities[specimen[0][i]], cities[specimen[0][i + 1]])

    track_len = track_len + distance.euclidean(cities['X'], cities[specimen[0][0]])
    track_len = track_len + distance.euclidean(cities['X'], cities[specimen[0][-1]])

    specimen[1] = track_len


def find_minimum(population):
    minimal_track = ["", 1000]

    for specimen in population:

        if minimal_track[1] > specimen[1]:
            minimal_track[1] = specimen[1]
            minimal_track[0] = specimen[0]

    print('Minimal length: %f' % minimal_track[1])
    print('Best track: X' + minimal_track[0] + 'X')

    return minimal_track


def evaluate(population):
    for specimen in population:
        count_length(country, specimen)

    return population


"""
========================================================================================
SELECTION - > choosing shortest tracks in tournaments
========================================================================================
"""


def tournament(population):
    players_index = random.sample(range(0, len(population) - 1), 2)
    players = [population[players_index[0]], population[players_index[1]]]

    if players[0][1] <= players[1][1]:
        winner = players[0]
    else:
        winner = players[1]

    return winner


def selection(population):
    new_population = []

    while len(new_population) != len(population):
        new_spec = tournament(population)
        new_population.append(new_spec)

    return new_population


"""
========================================================================================
MUTATION - > modificating a track
========================================================================================
"""


def mutation(population, chance, rang):
    mutants = random.sample(range(0, len(population) - 1), int(len(population) * chance))
    mut_range = int(rang * 29)

    ix = [random.randint(0, 29)]
    left_ix = ix[0] - mut_range

    if left_ix < 0:
        left_ix = ix[0] + abs(left_ix)

    ix.append(left_ix)

    for ind in mutants:
        mutant = list(population[ind][0])
        copy = mutant[ix[0]:ix[1]]
        random.shuffle(copy)
        mutant[ix[0]:ix[1]] = copy
        population[ind][0] = "".join(mutant)
        count_length(country, population[ind])


"""
========================================================================================
VISUALIZE A SOLUTION
========================================================================================
"""


def visualize_solution(minimal_track):
    seg = list(minimal_track[0])

    plt.figure(figsize=(10, 10))
    plt.xlim(-35, 30)
    plt.ylim(-35, 30)

    for city in country:
        plt.plot(country[city][0], country[city][1], marker='o', markersize=5, color='r')

    for i in range(0, 28):
        x_val = [country[seg[i]][0], country[seg[i+1]][0]]
        y_val = [country[seg[i]][1], country[seg[i + 1]][1]]

        plt.plot(x_val, y_val, color='r')

    for i in range(-1, 1):
        x_val = [country['X'][0], country[seg[i]][0]]
        y_val = [country['X'][1], country[seg[i]][1]]

        plt.plot(x_val, y_val, color='r')

    plt.plot(0, 0, marker='*', markersize=10, color='g')
    plt.show()


"""
========================================================================================
ANALYSING ALGORITHM
========================================================================================
"""

check_args()
random.seed = 300392

map_1 = {'X': [0, 0], 'A': [0, -20], 'a': [0, -10], 'B': [0, 10], 'b': [0, 20],
         'x': [10, 0], 'C': [10, -20], 'c': [10, -10], 'D': [10, 10], 'd': [10, 20],
         'Y': [20, 0], 'E': [20, -20], 'e': [20, -10], 'F': [20, 10], 'f': [20, 20],
         'y': [-10, 0], 'G': [-10, -20], 'g': [-10, -10], 'H': [-10, 10], 'h': [-10, 20],
         'Z': [-20, 0], 'I': [-20, -20], 'i': [-20, -10], 'J': [-20, 10], 'j': [-20, 20],
         'z': [-30, 0], 'K': [-30, -20], 'k': [-30, -10], 'L': [-30, 10], 'l': [-30, 20]}

map_2 = {'X': [0, 0], 'A': [5, -2], 'a': [11, 4], 'B': [17, 16], 'b': [-13, 3],
         'x': [7, 15], 'C': [8, -25], 'c': [9, -10], 'D': [10, 1], 'd': [1, 2],
         'Y': [-11, 26], 'E': [-18, 16], 'e': [0, -7], 'F': [6, 12], 'f': [20, 7],
         'y': [-4, 10], 'G': [-12, 8], 'g': [-1, 12], 'H': [-8, 10], 'h': [-10, 21],
         'Z': [2, 17], 'I': [-22, -3], 'i': [6, 9], 'J': [2, 14], 'j': [-20, 15],
         'z': [-3, 5], 'K': [-7, -2], 'k': [15, -10], 'L': [-7, 19], 'l': [-5, 2]}

map_3 = {'X': [0, 0], 'A': [-15, 21], 'a': [-18, 26], 'B': [-17, 25], 'b': [-17.5, 28],
         'x': [-16.9, 17.8], 'C': [-25, 19], 'c': [-15, 28], 'D': [-20, 25.5], 'd': [-21, 21.5],
         'Y': [17, 5], 'E': [18.5, 4.9], 'e': [18, 3.25], 'F': [21, 1.85], 'f': [20.1, 2.1],
         'y': [20.7, 5], 'G': [15, 7], 'g': [16.3, 1.5], 'H': [12.7, 6], 'h': [13, 2.8],
         'Z': [0, -28], 'I': [1.5, -19], 'i': [-2.5, -20], 'J': [-1, -24], 'j': [2, -20.75],
         'z': [0.9, -18], 'K': [1.75, -22], 'k': [-1.6, -18.9], 'L': [3, -17.5], 'l': [2.4, -18.8]}

maps = [map_1, map_2, map_3]

country = maps[int(sys.argv[1])]
population_size = int(sys.argv[2])
mutation_chance = float(sys.argv[3])
mutation_range = float(sys.argv[4])

# ========================================================================================

start_time = time.perf_counter_ns()
main_population = generate_population(population_size)
evaluate(main_population)

for x in range(0, 20000):
    selection(main_population)
    mutation(main_population, mutation_chance, mutation_range)

evaluate(main_population)
duration = (time.perf_counter_ns() - start_time) / (10 ** 9)

print('After optimalization:')
minimum = find_minimum(main_population)
print('Elapsed time: %f s' % duration)

# ========================================================================================

visualize_solution(minimum)
