# Travelling salesman problem

The main task is to find the shortest way for salesman, which allows him to visit all of the 30 cities on the map during his travel (each city can be seen by him only **once**). The answer is the sequence of those cities, in which they should be visited to prevent from making cyclic graphs by salesman. 

[Some theory about TSP](https://en.wikipedia.org/wiki/Travelling_salesman_problem)

To solve this kind of problem, evolutionary algorithms can be used. They operates on parameters like: population's size, chance of single specimen's mutation and its range. The purpose of this algorithm is to minimaze value of travel's length, so to find the shortest path between all vertices in a graph.

[Some theory about evolutionary algorithms](https://towardsdatascience.com/introduction-to-evolutionary-algorithms-a8594b484ac)

## Requirements

    Python 3.8.8 (or higher)
    module matplotlib 3.3.4.
    module scipy 1.6.1.

## How to run

    > python3 ./evolutionary.py map size mut_chance mut_range

    Arguments:
        map - choosen map (city distribution) <int>
            0 - regular
            1 - random
            2 - groups of cities
        size - size of population <int>
        mut_chance - probability of being mutated <float>
        mut_range - range of mutation/number of changed cities <float>

## Available maps 

Single map always contains **_30 cities_**, but their arrangement may differ, depending on choosen `map` argument value: 

### **_First map - Regular_**  

Choose `map = 0`

![regular map](img/regular.png)

### **_Second map - Random_**  

Choose `map = 1`

![random map](img/random.png)

### **_Third map - Group of cities_**  

Choose `map = 2`

![groups map](img/groups.png)

## Conclusions

The best results can be reached using values:  
- size = 300  
- mut_chance = 0.01 (1% chance of mutation)  
- mut_size = 0.1 (10% of genes changed in single mutation)

Size of population should be significantly bigger than number of cities (vertices in graph). Small value may cause stopping in local minimum. On the other hand, too many speciman make this algorithm too random.

High mutation chance makes evolutionary algorithm too random, so . The most beneficial value is equal to **1%**, because it prevents from stucking in local minimum. 

Also mutation size should not be high. Changing too many genes in a single specimen make this algorithm non-deterministic and non-related with it's idea. 